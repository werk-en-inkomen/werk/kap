const simplePlantUML = require("@akebifiky/remark-simple-plantuml");

module.exports = {
  title: 'Klantbeeld, Advies, Profiel',
  tagline: 'C4 Model',
  url: 'https://gitlab.com/werk-en-inkomen/werk/kap',
  baseUrl: '/werk/kap/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'marc4gov',
  projectName: 'werk-kap',
  themeConfig: {
    navbar: {
      title: 'Werk: Klantbeeld - Advies - Profiel (KAP)',
      logo: {
        alt: 'My Site Logo',
        src: 'img/TPB.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {
          href: 'https://gitlab.com/werk-en-inkomen/werk/kap',
          label: 'GitHub',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'C4 Model',
              to: 'docs/',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/docusaurus',
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} My Project, Inc. Built with Docusaurus.`,
    },
  },
  plugins: [],
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          remarkPlugins: [simplePlantUML],
          editUrl:
            'https://gitlab.com/werk-en-inkomen/werk/kap/-/edit/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
