module.exports = {
  someSidebar: {
    Introduction: ['intro'],
    'C4 Model': ['c4m/c01', 'c4m/c02', 'c4m/c03', 'c4m/c04', 'c4m/c05', 'c4m/c06', 'c4m/c07'],
    Features: ['mdx'],
  },
};
