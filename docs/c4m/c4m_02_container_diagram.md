---
id: c02
title: Container diagram
---

> A container diagram zooms in on software systems, with a high-level overview of the technical building blocks.
>  A "container" could be a server-side web application, SPA, desktop application, mobile app, database schema, file system, etc. A container is a runnable/deployable unit.

KAP 

```plantuml
@startuml "KAP"
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_TOP_DOWN()

title Container Context diagram for KAP

Person_Ext(anonymous_user, "Burger")

System_Ext(instrumenten, "Instrumenten", "Instrumenten")
System_Ext(plusmin, "plusmin", "plusmin")
System_Ext(ssi_wallet, "SSI Wallet", "Private credential store", "wallet")

System_Boundary(c1, "KAP"){
    Container(Triage, "Triage", "", "", "")
    Container(Competentiemeter, "Competentiemeter API", "", "", "")
    
    Container(Wensberoepen, "Wensberoepen API", "", "", "")
    Container(Profielverbeteraar, "Profielverbeteraar API", "", "", "")
}

Rel_L(anonymous_user, ssi_wallet, "Discloses and stores credentials")

Rel_D(ssi_wallet, Triage, "cv", "")
Rel_U(Triage, ssi_wallet, "Competenties", "")

Rel_D(ssi_wallet, Competentiemeter, "werkervaring CV", "")
Rel_U(Competentiemeter, ssi_wallet, "competenties", "")

Rel_D(Triage, Wensberoepen, "Wat wil je doen?", "")
Rel_D(Competentiemeter, Profielverbeteraar, "Competenties huidig", "")

Rel_R(Profielverbeteraar, instrumenten, "", "")
Rel_R(Profielverbeteraar, plusmin, "fit/gap", "")

@enduml
```


