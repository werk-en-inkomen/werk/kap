---
id: c03
title: Component diagram
---

> A component diagram zooms into an individual container, showing the components inside of it.

KAP 

```plantuml
@startuml "KAP"

!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Component.puml


title System Context diagram for Klant advies profiel (KAP)

Component(CV, "CV", "")
Component(Competentie, "Competentie", "")
ContainerDb(Database, "Database", "")

Rel_R(CV, Competentie, "inladen")
Rel_L(Competentie, CV, "Competenties")
Rel_R(Competentie, Database, "opvragen")

@enduml
```


