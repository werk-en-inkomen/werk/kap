---
id: c01
title: System Context
sidebar_label: System Context
slug: /c4m/system_context
---



> A system context diagram is the starting point for the system's place in the world it operates in.
>
> Detail is not important here for this is the bigger picture. Focus is on people (actors, roles, personas, etc.) and on software systems to be interpreted by non-technical people

```plantuml
@startuml "KAP"
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Container.puml

LAYOUT_TOP_DOWN()

title System Context diagram for Klant advies profiel (KAP)

Person(administration_user, "Burger", "Inwoner")
Person_Ext(anonymous_user, "Transactiepartijen", "Gemeente/HV")
System(KAP, "KAP", "DV-keten")

Rel_R(administration_user, KAP, "")
Rel_L(anonymous_user, KAP, "")

@enduml
```

